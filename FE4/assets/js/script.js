var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
      labels: ['Category 1', 'Category 2', 'Category 3', 'Category 4'],
      datasets: [{  
        label: 'Fiction',
        backgroundColor: 
            '#305080',
        data: [42, 33, 45, 43],  
      },{  
        label: 'Action',
        backgroundColor: 
            '#2F67BD',
        data: [32, 42, 29, 34,],
      },{  
        label: 'Mystery',
        backgroundColor: 
            '#7095CD',
        data: [30, 30, 40, 45]
      }]
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
            max: 50,
            display:false
        },
    }],
    xAxes: [{
      ticks: {
          max: 70
      },
      gridLines:{
        display: false
      }
    }],
   },
   
    title: {
      display: true,
      text: 'Borrowed and Returned Books',
      fontSize: '18',
      fontFamily: 'Poppins',
    },
    legend:{
      position: 'bottom',
      align: 'center'
    },
    gridLines: {
      display: false
   },
  }
});


var ctx = document.getElementById('myChart2').getContext('2d');
var myChart2 = new Chart(ctx, {
  type: 'pie',
  data: {
      labels: ['Fiction', 'Action', 'Mystery'],
      datasets: [{  
        label: 'Ambot',
        backgroundColor: 
            ['#305080',
            '#2F67BD',
            '#7095CD'],
        data: [80, 30, 15]
      }  
      ]
  },
 options: {
    scales: {
        yAxes: [{
            ticks: {
                max: 70,
                display: false
            },
            gridLines:{
                display:false
            }
        }]
      },
      title: {
        display: true,
        text: 'Popular Book Genre',
        fontSize: '18',
        fontFamily: 'Poppins',
    },
    legend:{
      position: 'bottom',
      align: 'center'
    },
    gridLines: {
      display: false
   },
   
  }
});

