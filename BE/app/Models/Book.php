<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'author', 'copies','category_id'];

    public function category(){
        return $this->hasOne(Category::class,'id', 'category_id');
    }
    public function borrowed(){
        return $this->hasMany(BorrowedBook::class,'id', 'book_id');
    }
    public function returned(){
        return $this->hasMany(ReturnedBook::class,'book_id', 'id');
    }
}
