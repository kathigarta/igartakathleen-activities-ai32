<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Patron;
use App\Http\Controllers\Controller;
use App\Http\Requests\StorePatronRequest;

class PatronController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePatronRequest $request)
    {
        Patron::create($request->validated());
        return response()->json(['message' => 'Patron has been added.', 'patron' => $patron], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(Patron::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePatronRequest $request, $id)
    {
        Patron::where('id', $id)->update($request->all());
        $patron->update($request->validated());
        return response()->json(['message' => 'Patron has been updated.', 'patron' => $patron]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Patron::where('id', $id)->delete();
        $patron->delete();
        return response()->json(['message' => 'Patron has been deleted.']);
    }
}
