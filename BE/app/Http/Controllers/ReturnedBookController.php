<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use App\Models\BorrowedBook;
use App\Models\ReturnedBook;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreReturnedBookRequest;

class ReturnedBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(ReturnedBook::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreReturnedBookRequest $request)
    {
        ReturnedBook::create($request->validated());
        $book = Book::find($request->book_id);
        Book::where('id', $request->book_id)->update(['copies' => $book->copies + $request->copies]);
        ReturnedBook::create($request->all());
        $this->destroy($request->patron_id);
        return response()->json(['message' => 'Book has been returned.']);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BorrowedBook::where('patron_id', $id)->delete();
    }
}
