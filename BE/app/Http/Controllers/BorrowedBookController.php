<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use App\Models\BorrowedBook;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBorrowedBookRequest;

class BorrowedBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(BorrowedBook::with(['patron', 'book', 'book.category'])->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBorrowedBookRequest $request)
    {
        $create_borrowed_book = BorrowedBook::create($request->only(['book_id', 'copies', 'patron_id']));
        $borrowed_book = BorrowedBook::with(['book'])->find($create_borrowed_book->id);
        $borrowed_book->book->update(['copies' => $borrowed_book->book->copies - $request->copies]);
        return response()->json(['message' => 'Book has been borrowed', 'borrowed' => $borrowed_book]);
    }
}
