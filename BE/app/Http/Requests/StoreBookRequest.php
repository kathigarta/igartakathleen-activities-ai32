<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\BorrowedBook;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class StoreBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [        
            'name' => 'required|min:2|max:100', 
            'author' => 'required|min:1|max:5',
            'copies' => 'required|numeric',
            'category_id' => 'required|exists:categories,id'  
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function message()
    {
        return [        
            'name.required' => 'Name is required.',
            'name.min' => 'Name must have at least minimum of 2 characters',
            'name.max' => 'Name must not exceed 100 maximum of characters',
            'autor.required' => 'Author is required',
            'author.min' => 'Author must have at least minimum of 2 characters',
            'author.max' => 'Author must not exceed 100 maximum of characters',
            'copies.required' => 'Copies is required.',
            'copies.numeric' => 'Invilid input. Copies must only be a number.',
            'category_id.required' => "Book must belong to a category",
            'category_id.exists' => "Category does not exist"
        ];
    }

    //Display error message
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
