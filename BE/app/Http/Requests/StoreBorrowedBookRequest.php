<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreBorrowedBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $book = Book::find(request()->get('book_id'));
            if(!empty($book)){
                $copies = $book->copies;
            }
            else{
                $copies = request()->get('copies');
            }

        return [
            'patron_id' => 'required|',
            'copies.lte' => 'Copies have exceeded the total copies of books',
            'book_id' => 'required|exists:patrons,id'
        ];
    }

     /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function message()
    {
        return [        
            'patron_id.exists' => 'Patron does not exist',
            'copies.lte' => 'Copies exceeded the total copies of book',
            'book_id.exists' => 'Book does not exist'
        ];
    }

    //Display error message
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
