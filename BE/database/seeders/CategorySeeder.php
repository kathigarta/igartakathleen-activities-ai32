<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $categories=['Fiction', 'Mystery', 'Action'];

        foreach($categories as $category){
            Category::create(['category'=>$category]);
        }
    }
}
