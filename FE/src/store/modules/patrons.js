import axios from './config/axios';

export default{
    state:{
      patrons: [],
    },
    getters:{
        allPatrons: state=> state.patrons
    },
    mutations:{
      setPatrons: (state, patrons) => state.patrons = patrons,
      DeletePatron: (state, id) => (state.patrons = state.patrons.filter((patron) => patron.id !== id)),
      AddPatron: (state, patron) => state.patrons.unshift({ ...patron }),
      UpdatePatron: (state, updatePatron) => {
        const index = state.patrons.findIndex((patron) => patron.id === updatePatron.id);
        if (index !== -1) {
          state.patrons.splice(index, 1, { ...updatePatron });
        }
      },

    },
    actions:{
      async addPatron({commit}, patron){
        await axios
        .post("/patrons", patron)
        .then((res) => {
          commit("AddPatron", res.data.patron);
          toastr.success(res.data.message, "Add Successful");
        })
        .catch((err) => {
          if (err.response.status == 422) {
            for (var i in err.response.data.errors) {
              toastr.error(err.response.data.errors[i][0], "Add Failed");
            }
          }
        });

      },
      async fetchPatron({commit}){
        await axios.get()
        .get("/patrons")
          .then((res) => {
            commit("setPatrons", res.data);
          })
          .catch((err) => {
            console.error(err);
          });
          
      },
      async updatePatron({commit}, patron){
        var id = patron.id
        var body = {
          last_name: patron.last_name,
          first_name: patron.first_name,
          middle_name: patron.middle_name,
          email: patron.email
        }
  
        await axios
          .put(`/patrons/${id}`, body)
          .then((res) => {
            toastr.success(res.data.message, "Update Successful");
            commit("UpdatePatron", res.data.patron);
          })
          .catch((err) => {
            if (err.response.status == 422) {
              for (var i in err.response.data.errors) {
                toastr.error(err.response.data.errors[i][0], "Update Failed");
              }
            }
          });
    },
      },
      async deletePatron({commit}, id){
        await axios.delete(`/patrons/${id}`)
        .then(res => {
          commit("DeletePatron", id);
          toastr.success(res.data.message, "Delete Successful");
        })
        .catch(err => {
          console.error(err);
        })
      },
} 
