//import axios from './config/axios';

export default{
    state:{
        borrowed: [],
    },
    getters:{
        allBorrowedBooks: state=> state.borrowedbooks
    },
    mutations:{
        BorrowedBook: (state, borrowed) => state.borrowed.push({ ...borrowed }),
        BorrowedBooks: (state, borrowedBooks) =>
          (state.borrowed = borrowedBooks),
    },
    
    actions:{
        async borrowedBook({ commit }, borrow) {
            await axios
              .post("/borrowedbooks", borrow)
              .then((res) => {
                toastr.success(res.data.message, "Borrow Successfully");
                commit("BorrowedBook", res.data.borrowed);
              })
              .catch((err) => {
                if (err.response.status == 422) {
                  for (var i in err.response.data.errors) {
                    toastr.error(err.response.data.errors[i][0], "Borrow Failed");
                  }
                }
              });
          },
          async fetchBorrowedBooks({ commit }) {
            await axios
              .get("/borrowedbooks")
              .then((res) => {
                commit("BorrowedBooks", res.data);
              })
              .catch((err) => {
                console.error(err);
              });
          },
    },
} 