import axios from './config/axios';

export default{
    state:{
        returnedbooks: []
    },
    getters:{
       
    },
    mutations:{
        mutations: {
            BorrowedBook: (state, returned) => state.returned.push({ ...returned }),
            BorrowedBooks: (state, returnedbooks) => (state.returned = returnedbooks),
          },
    },
    
    actions:{
        async returnedBook({ commit }, returned) {
            await axios
              .post("/returnedbooks", returned)
              .then((res) => {
                toastr.success(res.data.message, "Return Successfully");
                commit("ReturnedBook", res.data.returned);
              })
              .catch((err) => {
                if (err.response.status == 422) {
                  for (var i in err.response.data.errors) {
                    toastr.error(err.response.data.errors[i][0], "Return Failed");
                  }
                }
              });
          },
        },
} 