import axios from './config/axios';

export default{
    state:{
        books: []
    },
    getters:{
        allBooks: state=> state.books
    },
    mutations:{
        setBooks: (state, books) => (state.books = books),
        DeleteBook: (state, id) =>
        (state.books = state.books.filter((book) => book.id !== id)),
        NewBook: (state, book) => state.books.unshift({ ...book }),
        UpdateBook: (state, updatedBook) => {
        const index = state.books.findIndex((book) => book.id === updatedBook.id);
        if (index !== -1) {
        state.books.splice(index, 1, { ...updatedBook });
      }
    },
    },
    actions:{
        async addBook({ commit }, book) {
            await axios
              .post("/books", book)
              .then((res) => {
                commit("NewBook", res.data.book);
                toastr.success(res.data.message, "Add Successful");
              })
              .catch((err) => {
                if (err.response.status == 422) {
                  for (var i in err.response.data.errors) {
                    toastr.error(err.response.data.errors[i][0], "Add Failed");
                  }
                }
              });
          },
          async deleteBook({ commit }, id) {
            await axios.delete(`/books/${id}`)
            .then(res => {
              commit("DeleteBook", id);
              toastr.success(res.data.message, "Delete Successful");
            })
            .catch(err => {
              console.error(err.response.data);
            })
      
          },
          async fetchBooks({ commit }) {
            await axios
              .get("/books")
              .then((res) => {
                commit("setBooks", res.data);
              })
              .catch((err) => {
                console.error(err);
              });
              
          },
          async updateBook({ commit }, book) {

            var id = book.id
            var body = {
              name: book.name,
              author: book.author,
              copies: book.copies,
              category_id: book.category_id
            }
      
            await axios
              .put(`/books/${id}`, body)
              .then((res) => {
                toastr.success(res.data.message, "Update Successful");
                commit("UpdateBook", res.data.book);
              })
              .catch((err) => {
                if (err.response.status == 422) {
                  for (var i in err.response.data.errors) {
                    toastr.error(err.response.data.errors[i][0], "Update Failed");
                  }
                }
              });
          },
    },
} 